# WSL2 Ubuntu 22.04 
 
## Write 20%, Read 80%

```console
## Rust 1.64.0 - W: 20%, R: 80% - std::sync::RwLock

$ cargo run --release
num_segments: 1, elapsed: 48.601253329s, sum: 49892486945
num_segments: 2, elapsed: 32.306936951s, sum: 49883352298
num_segments: 4, elapsed: 15.133929928s, sum: 49879557677
num_segments: 8, elapsed: 7.619652924s, sum: 49888887798
num_segments: 16, elapsed: 4.828090113s, sum: 49868556009
num_segments: 32, elapsed: 3.622695245s, sum: 49877710822
num_segments: 64, elapsed: 2.956921687s, sum: 49882641897

## Rust 1.64.0 - W: 20%, R: 80% - parking_lot::RwLock

$ cargo run --release --features parking_lot
num_segments: 1, elapsed: 276.638251848s, sum: 49901152702
num_segments: 2, elapsed: 145.856171054s, sum: 49893666522
num_segments: 4, elapsed: 75.722236841s, sum: 49889943292
num_segments: 8, elapsed: 36.854657856s, sum: 49895917728
num_segments: 16, elapsed: 17.460915003s, sum: 49887064800
num_segments: 32, elapsed: 5.926523821s, sum: 49891720735
num_segments: 64, elapsed: 3.462247066s, sum: 49885066360

## Rust 1.61.0 - W: 20%, R: 80% - std::sync::RwLock

$ cargo +1.61.0 run --release
num_segments: 1, elapsed: 233.400081812s, sum: 49895252512
num_segments: 2, elapsed: 207.072674794s, sum: 49902686669
num_segments: 4, elapsed: 119.812300302s, sum: 49886699228
num_segments: 8, elapsed: 71.109674920s, sum: 49893986019
num_segments: 16, elapsed: 41.308190849s, sum: 49874715987
num_segments: 32, elapsed: 24.652401516s, sum: 49892741535
num_segments: 64, elapsed: 18.136293187s, sum: 49886271356
```

## Write 50%, Read 50%

```console
## Rust 1.64.0 - W: 50%, R: 50% - std::sync::RwLock

$ cargo run --release
num_segments: 1, elapsed: 65.212193841s, sum: 31186751170
num_segments: 2, elapsed: 43.503927935s, sum: 31178822907
num_segments: 4, elapsed: 22.597711001s, sum: 31178835785
num_segments: 8, elapsed: 9.307396927s, sum: 31177439305
num_segments: 16, elapsed: 5.524862398s, sum: 31176574427
num_segments: 32, elapsed: 3.745576460s, sum: 31189509004
num_segments: 64, elapsed: 3.177323606s, sum: 31179887038

## Rust 1.64.0 - W: 50%, R: 50% - parking_lot::RwLock

$ cargo run --release --features parking_lot
num_segments: 1, elapsed: 318.368479446s, sum: 31183651747
num_segments: 2, elapsed: 168.837448677s, sum: 31186771073
num_segments: 4, elapsed: 81.514202860s, sum: 31186740399
num_segments: 8, elapsed: 40.413141520s, sum: 31187811284
num_segments: 16, elapsed: 16.484305913s, sum: 31183716971
num_segments: 32, elapsed: 7.133899554s, sum: 31180976560
num_segments: 64, elapsed: 3.665767233s, sum: 31182952256

## Rust 1.61.0 - W: 50%, R: 50% - std::sync::RwLock

$ cargo +1.61.0 run --release
num_segments: 1, elapsed: 236.177307506s, sum: 31181272513
num_segments: 2, elapsed: 278.908891677s, sum: 31182343212
num_segments: 4, elapsed: 163.244445052s, sum: 31181666780
num_segments: 8, elapsed: 94.723161427s, sum: 31180791380
num_segments: 16, elapsed: 58.593897124s, sum: 31182687428
num_segments: 32, elapsed: 43.287560610s, sum: 31183707843
num_segments: 64, elapsed: 40.486116462s, sum: 31184799595
```
