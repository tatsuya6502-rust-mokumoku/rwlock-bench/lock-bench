# Windows 11

## Write 20%, Read 80%

```console
## Rust 1.64.0 - W: 20%, R: 80% - std::sync::RwLock

$ cargo run --release
num_segments: 1, elapsed: 50.659564700s, sum: 49886373355
num_segments: 2, elapsed: 35.135675500s, sum: 49873571586
num_segments: 4, elapsed: 21.452109000s, sum: 49879980411
num_segments: 8, elapsed: 10.120891100s, sum: 49879745315
num_segments: 16, elapsed: 6.062306200s, sum: 49883567891
num_segments: 32, elapsed: 4.139069700s, sum: 49892761835
num_segments: 64, elapsed: 3.338963000s, sum: 49891427330

## Rust 1.64.0 - W: 20%, R: 80% - parking_lot::RwLock

$ cargo run --release --features parking_lot
num_segments: 1, elapsed: 176.117219500s, sum: 49895830391
num_segments: 2, elapsed: 95.162105400s, sum: 49895803255
num_segments: 4, elapsed: 50.496280800s, sum: 49895915421
num_segments: 8, elapsed: 20.966339200s, sum: 49891133434
num_segments: 16, elapsed: 5.339869500s, sum: 49885081055
num_segments: 32, elapsed: 3.630619300s, sum: 49887978509
num_segments: 64, elapsed: 3.094835100s, sum: 49882962850

## Rust 1.61.0 - W: 20%, R: 80% - std::sync::RwLock

$ cargo +1.61.0 run --release
num_segments: 1, elapsed: 57.631571400s, sum: 49885473044
num_segments: 2, elapsed: 32.287025500s, sum: 49885010312
num_segments: 4, elapsed: 19.179667200s, sum: 49870505779
num_segments: 8, elapsed: 10.108692900s, sum: 49881613383
num_segments: 16, elapsed: 6.674101400s, sum: 49867821371
num_segments: 32, elapsed: 4.343013900s, sum: 49881153947
num_segments: 64, elapsed: 3.326780900s, sum: 49887515640
```

## Write 50%, Read 50%

```console
## Rust 1.64.0 - W: 50%, R: 50% - std::sync::RwLock

$ cargo run --release
num_segments: 1, elapsed: 59.164095400s, sum: 31181696145
num_segments: 2, elapsed: 34.407330400s, sum: 31185903899
num_segments: 4, elapsed: 23.181846300s, sum: 31185046204
num_segments: 8, elapsed: 11.954953900s, sum: 31182123806
num_segments: 16, elapsed: 7.084494300s, sum: 31187323746
num_segments: 32, elapsed: 4.793522500s, sum: 31188040291
num_segments: 64, elapsed: 3.891979700s, sum: 31180207989

## Rust 1.64.0 - W: 50%, R: 50% - parking_lot::RwLock

$ cargo run --release  --features parking_lot
num_segments: 1, elapsed: 194.520897700s, sum: 31186066287
num_segments: 2, elapsed: 100.603123900s, sum: 31186598207
num_segments: 4, elapsed: 51.079087000s, sum: 31184700773
num_segments: 8, elapsed: 27.133603300s, sum: 31182762106
num_segments: 16, elapsed: 7.266366500s, sum: 31178395057
num_segments: 32, elapsed: 3.968315200s, sum: 31183267415
num_segments: 64, elapsed: 3.302045300s, sum: 31178903665
```
