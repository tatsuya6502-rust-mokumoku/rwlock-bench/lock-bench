# Lock Bench

## Running Benchmarks

```console
$ cargo run --release
$ cargo run --release --features parking_lot

## On Linux
$ cargo run +1.61.0 --release
```

## License

The MIT License.
