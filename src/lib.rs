use std::sync::Arc;

use rand::{rngs::ThreadRng, thread_rng, Rng};

pub mod map;

pub fn run_test(segment_bits: u8, num_threads: usize, num_ops: usize, num_keys: usize) -> usize {
    let map = Arc::new(map::SegmentedHashMap::new(segment_bits));
    let threads = (0..num_threads)
        .map(|n| {
            let my_map = Arc::clone(&map);
            std::thread::spawn(move || do_work(n, &my_map, num_ops, num_keys))
        })
        .collect::<Vec<_>>();

    threads
        .into_iter()
        .map(|t| t.join().unwrap())
        .sum::<usize>()
}

fn do_work(
    id: usize,
    map: &map::SegmentedHashMap<usize, usize>,
    num_ops: usize,
    num_keys: usize,
) -> usize {
    let mut rng = thread_rng();
    let mut sum = 0;

    for i in 0..num_ops {
        let key = rng.gen_range(0..num_keys);

        match rng.gen_range(0..=4) {  // 20:80
        // match rng.gen_range(0..=1) {  // 50:50 
            // Write: 20%
            0u8 => {
                map.insert(key, id);
            }
            // Read: 80%
            1.. => {
                if let Some(v) = map.get(&key) {
                    sum += v;
                };
            }
        }

        back_off(i, &mut rng);
    }

    sum
}

fn back_off(i: usize, rng: &mut ThreadRng) {
    if i % 100 == 0 {
        std::thread::yield_now();
    } else {
        let delay = rng.gen_range(0..5);
        for _ in 0..delay {
            std::hint::spin_loop();
        }
    }
}
