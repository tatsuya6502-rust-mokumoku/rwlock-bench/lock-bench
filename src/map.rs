use std::{
    borrow::Borrow,
    collections::{hash_map::RandomState, HashMap},
    hash::{BuildHasher, Hash, Hasher},
};

#[cfg(feature = "parking_lot")]
use parking_lot::RwLock;

#[cfg(not(feature = "parking_lot"))]
use std::sync::RwLock;

pub struct SegmentedHashMap<K, V> {
    segments: Box<[RwLock<HashMap<K, V>>]>,
    segment_mask: u64,
    hasher: RandomState,
}

impl<K, V> SegmentedHashMap<K, V>
where
    K: Eq + Hash,
    V: Clone,
{
    pub fn new(segment_bits: u8) -> Self {
        let segment_count = 1 << segment_bits;
        let segments = (0..segment_count)
            .map(|_| Default::default())
            .collect::<Vec<_>>()
            .into_boxed_slice();
        Self {
            segments,
            segment_mask: (segment_count - 1) as u64,
            hasher: RandomState::default(),
        }
    }

    pub fn insert(&self, key: K, value: V) -> Option<V> {
        let hash = self.hash(&key);
        let segment = &self.segments[self.segment_index(hash)];

        #[cfg(feature = "parking_lot")]
        let maybe_v = segment.write().insert(key, value);
        #[cfg(not(feature = "parking_lot"))]
        let maybe_v = segment.write().unwrap().insert(key, value);
        maybe_v
    }

    pub fn get<Q>(&self, key: &Q) -> Option<V>
    where
        K: Borrow<Q>,
        Q: Eq + Hash,
    {
        let hash = self.hash(&key);
        let segment = &self.segments[self.segment_index(hash)];

        #[cfg(feature = "parking_lot")]
        let maybe_v = segment.read().get(key).map(Clone::clone);
        #[cfg(not(feature = "parking_lot"))]
        let maybe_v = segment.read().unwrap().get(key).map(Clone::clone);
        maybe_v
    }

    fn hash<Q>(&self, key: &Q) -> u64
    where
        K: Borrow<Q>,
        Q: Eq + Hash,
    {
        let mut hasher = self.hasher.build_hasher();
        key.hash(&mut hasher);
        hasher.finish()
    }

    fn segment_index(&self, hash: u64) -> usize {
        (hash & self.segment_mask) as usize
    }
}
