use std::time::Instant;

const NUM_THREADS: usize = 500;
const NUM_OPS: usize = 500_000;
const NUM_KEYS: usize = 10_040;

fn main() {
    let mut is_first_loop = true;
    // 0 bit:   1 segment
    // 4 bits: 16 segments
    // 6 bits: 64 segments
    for segment_bits in 0..=6 {
        if is_first_loop {
            is_first_loop = false;
        } else {
            // Cool down the processors.
            std::thread::sleep(std::time::Duration::from_secs(10));
        }

        let start = Instant::now();
        let sum = lock_bench::run_test(segment_bits, NUM_THREADS, NUM_OPS, NUM_KEYS);
        let elapsed = start.elapsed();
        println!(
            "num_segments: {}, elapsed: {}.{:09}s, sum: {}",
            2usize.pow(segment_bits as u32),
            elapsed.as_secs(),
            elapsed.subsec_nanos(),
            sum,
        );
    }
}
